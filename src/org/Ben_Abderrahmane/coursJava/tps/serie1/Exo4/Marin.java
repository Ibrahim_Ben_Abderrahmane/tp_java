package org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4;

import java.io.Serializable;

/***************************** BEN ABDERRAHMANE IBRAHIM ****************************/
public class Marin implements Cloneable,Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		//Modification apport�e � la classe Marin pour qu'elle soit clonable
		public Object clone() throws CloneNotSupportedException{
		    try{  
		        return super.clone();  
		    }catch(Exception e){ 
		        return null; 
		    }
		}
	/*declaration des champs de la calsse Marin*/

	private String nom,prenom;
	public int salaire;
/************************************************************************************/		
			/*Creation du constructeur qui initialise le champs marin*/
						/* 1er constructeur de la classe Marin*/
/************************************************************************************/	
	public Marin (String nouveauNom, String nouveauPrenom, int nouveauSalaire) { 
		nom = nouveauNom ;
		prenom = nouveauPrenom ;
		salaire = nouveauSalaire ;
	}
/************************************************************************************/	
							/*2EME Constructeur*/																																
/************************************************************************************/	
	public Marin (String nouveauNom, int nouveauSalaire) { 
		this (nouveauNom, " ", nouveauSalaire);
	}
/************************************************************************************/
	/*les geters et les seters pour les champs nom prenom et salaire*/
/************************************************************************************/
	/*pour le champs NOM*/
	public void setNom (String nom) { 
		this.nom = nom;
	}
	public String getNom () { 
		return this.nom;
	}
	/*pour le champs Prenom*/
	public void setPrenom (String prenom) { 
		this.prenom = prenom;
	}
	
	public String getPrenom () { 
		return this.prenom;
	}
	/*pour le champ Salaire*/
	public void setSalaire (int salaire) { 
		this.salaire = salaire;
	}
	public int getSalaire () { 
		return this.salaire;
	}
/************************************************************************************/
	/*La methode pour augmenter le salaire d'un marin*/
/************************************************************************************/
	public void augmenteSalaire (int augmentation) { 
		salaire = salaire +augmentation ;
	}
	
/************************************************************************************/
	/*La methode qui permet d'afficher les champs de la classe Marin*/
/************************************************************************************/
	@Override
	public String toString() {
		return "nom:" + nom + "\n prenom:" + prenom + "\n salaire:" + salaire+"\n";
	}
/************************************************************************************/
	
/************************************************************************************/
	/*public boolean equals(Marin marin){
		return (nom.equals(marin.nom) && prenom.equals(marin.prenom)&&
				salaire==marin.salaire);
	 }*/
	
/************************************************************************************/
	
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((nom == null) ? 0 : nom.hashCode());
	result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
	result = prime * result + salaire;
	return result;
}
	
	
	@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Marin other = (Marin) obj;
	if (nom == null) {
		if (other.nom != null)
			return false;
	} else if (!nom.equals(other.nom))
		return false;
	if (prenom == null) {
		if (other.prenom != null)
			return false;
	} else if (!prenom.equals(other.prenom))
		return false;
	if (salaire != other.salaire)
		return false;
	return true;
}
	
	public static void main(String[] args) {
/************************************************************************************/
		/*Cr�ation de Objets de Type Marin*/
/************************************************************************************/
		System.out.println("*********************************************\n");
		Marin m1= new Marin("IBRAHIM","BEN ABDERRAHMANE",3000);
		Marin m2= new Marin("IBRAHIM","BEN ABDERRAHMANE",3000);
		/* On peut aussi faire Marin m2=m1*/
		Marin m3= new Marin("Haguibou","TALL",30);
		
		/*AFFICHAGE du contenu marins*/
		System.out.println(m1.toString());
		System.out.println(m2.toString());
		System.out.println(m3.toString());
		System.out.println("*********************************************\n");
		
/************************************************************************************/
		/*Comparaison entre deux objet Marin*/
/************************************************************************************/
		System.out.println("*********************************************\n");
		
		if(m1.equals(m2)==true)
			System.out.println("m1 est egale � m2");
		else
			System.out.println("m1 est different de m2");
		
		if(m1.equals(m3)==true)
			System.out.println("m1 est egale � m3");
		else
			System.out.println("m1 est different de m3");
		
		if(m2.equals(m3)==true)
			System.out.println("m2 est egale � m3");
		else
			System.out.println("m2 est different de m3");
		System.out.println("*********************************************\n");
		
/************************************************************************************/
		/*affichage des codes hash
/************************************************************************************/
		System.out.println("*********************************************\n");
		System.out.println("le code de hashage de m1 est:" +m1.hashCode());
		System.out.println("le code de hashage de m2 est:" +m2.hashCode());
		System.out.println("le code de hashage de m3 est:" +m3.hashCode());
		System.out.println("*********************************************\n");
	}

}
