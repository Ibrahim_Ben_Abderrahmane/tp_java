package org.Ben_Abderrahmane.coursJava.tps.serie1.Exo5;

import org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4.Marin;

public class MarinUtil {
/************************************************************************************/
	/**
	 * augmentation des salaires des marins pass� en argument de la methode
	 * @param marins
	 * @param pourcentage
	 */
/************************************************************************************/
	static int i;
	public void augmenteSalaire(Marin[] marins, int pourcentage){
		for(i=0;i<marins.length;i++){
			marins[i].salaire = marins[i].salaire+ ((pourcentage*marins[i].salaire)/100);
		}
	}
/************************************************************************************/
	/**
	 * une methode qui retourne le salaire du marin le mieux pay�
	 * @param marins
	 * @return
	 */
/************************************************************************************/
	public static int getMaxSalaire (Marin[] marins){
		 int MaxSalaire = marins[0].salaire;
		for(i=0;i<marins.length;i++){
			if(MaxSalaire<marins[i].salaire){
				MaxSalaire=marins[i].salaire;
			}
		}
		return MaxSalaire;
	}
	
/*************************************************************************************/
	/**
	 * une methode qui retourne la moyenne des salaires
	 * @param marins
	 * @return
	 */
/*************************************************************************************/
	public static int getMoyenneSalaire (Marin[] marins){
		int MoySalaire=0;
		int i;
		for(i=0;i<marins.length;i++){
			MoySalaire=MoySalaire + marins[i].salaire;
			MoySalaire=MoySalaire/marins.length;
		}
		return MoySalaire;
	}
/*************************************************************************************/
	/**
	 * une methode qui permet de faire le tri
	 * @param marins
	 */
/*************************************************************************************/
	public static void triCroissant(Marin[] marins)
	{
		int i=0,j=0,tmp=0;
		for(i=0;i<marins.length;i++){
			for(j=1;j<marins.length;j++){
				if(marins[i].salaire<marins[j].salaire){
					tmp=marins[i].salaire;
					marins[i].salaire=marins[j].salaire;
					marins[j].salaire=tmp;
					j--;
				}
			}
		}
		tmp=marins[0].salaire;
		for(i=0;i<marins.length-1;i++){
			marins[i].salaire=marins[i+1].salaire;
		}
		marins[marins.length-1].salaire=tmp;
	}
/*************************************************************************************/
	/**
	 * 
	 * @param marins
	 * @return
	 */
/*************************************************************************************/
	public static int getMedianeSalaire (Marin[] marins){
		int index;
		if (marins.length%2==0){
			index=marins.length/2;
		}
		else{
			index=(marins.length-1)/2;
	
		}
		return marins[index].salaire;
	}
/*************************************************************************************/
	
	public static void main(String[] args) {
	int i;
	int tailleTableau=5;
	/*initialisation du tableau marins*/
	Marin [] marins= new Marin[tailleTableau];
		for(i=0;i<marins.length;i++){
			marins[i] =new Marin("nom","prenom",(i+1)*1000);
		}	
		
	/*affichage du salaire MAX et la moyenne*/
	System.out.println("*");
	System.out.println("le salaire maximum est: "+ getMaxSalaire(marins));
	System.out.println("la moyenne des salaires est: "+ getMoyenneSalaire(marins));

	
	triCroissant(marins);
	System.out.println("la mediane est: "+ getMedianeSalaire(marins));
	
	}
}
