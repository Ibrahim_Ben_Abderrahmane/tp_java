package org.Ben_Abderrahmane.coursJava.tps.serie5.Exo10;
/**
 * la deuxi�me version de la classr sauvegarde prend en argument une liste de 
 * marin contrairement � la premeier version qui prend un seul marin en param
 */
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4.Marin;
/**
 * 
 * @author Ben abderrahmane
 *
 */
public class SauvegardeVersion2 {
	/**
	 * 
	 * @param nom
	 * @return
	 */
	public static File createNewFile(String nom){
	File file=null;
	String str =nom+".txt";
	boolean bool = false;
	try{
		 file = new File(str); 
		 bool = file.exists();  
		 if(bool){ 
			 str = file.toString(); 
			 System.out.println("pathname string: "+str+"\n");
		 }
	}
	catch(Exception e){ 
		e.printStackTrace(); 
		 }
	return file;
	}
	/**
	 * 
	 * @param nomFichier
	 * @param marin
	 */
	public static void sauveFichierTexte(String nomFichier,List<Marin> marins_liste){
		String NewLigne=System.getProperty("line.separator");
		File file=null;
		file=createNewFile(nomFichier);
		try{
			Writer writer = new FileWriter(file);
			// �ecriture dans le fichier 
			for(int i=0;i<marins_liste.size();i++){
				writer.write(marins_liste.get(i).getNom()+"|"+marins_liste.get(i).getPrenom()+"|"+marins_liste.get(i).getSalaire()+NewLigne);
			}
			writer.close();
		}catch (IOException e){
			// affichage du message d�erreur et de la pile d�appel 
			System.out.println("Erreur " + e.getMessage());
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * @param nomFichier
	 * @return
	 */
	public static List<Marin> lisFichierText(String nomFichier){
		
		String str=nomFichier+".txt";
		StringTokenizer separator = null;
		Marin marin;
		List<Marin> marins_liste= new ArrayList<Marin>();
		try{
			InputStream in_put_stream=new FileInputStream(str); 
			InputStreamReader in_put_stream_reader=new InputStreamReader(in_put_stream);
			BufferedReader buffer_reader=new BufferedReader(in_put_stream_reader);
			String ligne;
			while ((ligne=buffer_reader.readLine())!=null){
				separator=new StringTokenizer(ligne,"|");
				marin=new Marin(separator.nextToken(),separator.nextToken(),Integer.parseInt(separator.nextToken()));
				marins_liste.add(marin);
			}
			buffer_reader.close(); 
			return marins_liste;
		}		
		catch (Exception e){
			System.out.println(e.toString());
			return null;
		}
	}
	/**
	 * 
	 * @param nomFichier
	 * @param marins_liste
	 */
	public static void sauveChampBinaire(String nomFichier,List<Marin> marins_liste){
		FileOutputStream file_out_put_stream = null;
		File file;
		String str=nomFichier+".bin";
		file=new File(str);
		try{
			file_out_put_stream=new FileOutputStream(file);
			DataOutputStream wr=new DataOutputStream(file_out_put_stream);
			for(int i=0;i<marins_liste.size();i++){
				wr.writeUTF(marins_liste.get(i).getNom());
				wr.writeUTF(marins_liste.get(i).getPrenom());
				wr.writeUTF(String.valueOf(marins_liste.get(i).getSalaire()));
			}
				wr.close();
		}
		catch(FileNotFoundException e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(file_out_put_stream !=null){
				try{
					file_out_put_stream.close();
				}
				catch (Exception e){
					System.out.println("Erreur"+e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}
	/**
	 * 
	 * @param nomFichier
	 * @return
	 */
	public static List<Marin> lisChampBinaire(String nomFichier){
		InputStream file_in_put_stream = null;
		DataInputStream data_in_put_stream=null;
		Marin marin;
		List<Marin> marins_liste= new ArrayList<Marin>();
		try{
			file_in_put_stream=new FileInputStream(nomFichier); 
			data_in_put_stream=new DataInputStream(file_in_put_stream);
			while (file_in_put_stream.available()>0){
				marin=new Marin(data_in_put_stream.readUTF(),data_in_put_stream.readUTF(),Integer.parseInt(data_in_put_stream.readUTF()));
				marins_liste.add(marin);
			}
			data_in_put_stream.close(); 
			return marins_liste;
		}
		catch (Exception e){
			System.out.println(e.toString());
			return null;
		}
	}
	/**
	 * 	
	 * @param nomFichier
	 * @param marin
	 */
	public static void sauveObject(String nomFichier,List<Marin> marins_liste) {
		File file=new File(nomFichier);
		ObjectOutputStream oos = null;
		try{
			oos=new ObjectOutputStream(new FileOutputStream(file));
			for(int i=0;i<marins_liste.size();i++){
				oos.writeObject(marins_liste.get(i));
			}
			
		}
		catch(FileNotFoundException e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(oos !=null){
				try{
					oos.close();
				}
				catch (Exception e){
					System.out.println("Erreur"+e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}
	/**
	 * 
	 * @param nomFichier
	 * @return
	 */
	public static List<Marin> lisObject(String nomFichier){
		ObjectInputStream ois = null;
		File file=new File(nomFichier);
		List<Marin> marins_liste= new ArrayList<Marin>();
		Marin marin;
		try{
			FileInputStream fis=new FileInputStream(file);
			ois=new ObjectInputStream(fis);
			while(fis.available()>0){
				marin= (Marin)ois.readObject();	
				marins_liste.add(marin);
			}
		}
		catch(FileNotFoundException e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(ois !=null){
				try{
					ois.close();
				}
				catch (Exception e){
					System.out.println("Erreur"+e.getMessage());
					e.printStackTrace();
				}
			}
		}
		return marins_liste;
	}
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) throws IOException {
		//creation de la liste marins
		List<Marin> marins_liste= new ArrayList<Marin>();
		Marin m1= new Marin("BEN ABDERRAHMANE","IBRAHIM",2000);
		Marin m2= new Marin("tall","haguibou",1800);
		Marin m3= new Marin("mukhtar","saqab",1900);
		Marin m4= new Marin("diallo","abdoulaye",2000);
		Marin m5= new Marin("diop","monzon",2100);
		marins_liste.add(m1);
		marins_liste.add(m2);
		marins_liste.add(m3);
		marins_liste.add(m4);
		marins_liste.add(m5);
		
		//test de la methode  sauveFichierTexte
		SauvegardeVersion2.sauveFichierTexte("Marins version2", marins_liste);
		
		//test de la methode  lisFichierText
		System.out.println("lecture du fichier text en cours...");
		System.out.println(SauvegardeVersion2.lisFichierText("Marins Version2"));
		
		//test de la methode  sauveChampBinaire
		SauvegardeVersion2.sauveChampBinaire("BinaryFileVersion2",marins_liste);
		
		//test de la methode  lisChampBinaire
		System.out.println("lecture du fichier binaire en cours...");
		System.out.println(SauvegardeVersion2.lisChampBinaire("BinaryFileVersion2.bin"));

		SauvegardeVersion2.sauveObject("fileobjectVersion2.data", marins_liste);
		
		//test de la methode  lisChampBinaire
		System.out.println("lecture du fichier fileObject...");
		System.out.println(SauvegardeVersion2.lisObject("fileobjectVersion2.data"));
	}

}
