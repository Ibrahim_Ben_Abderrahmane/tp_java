/********************************BEN ABDERRAHMANE IBRAHIM ************************/
package org.Ben_Abderrahmane.coursJava.tps.serie0.Exo0;
/**
 * 
 * @author Ben abderrahmane
 *
 */
public class Factorielle {
	
	/**
	 * 1ER Methode fonction reccurssive
	 * @param n
	 * @return
	 */
	public int factorielle(int n) {
	    if (n>1)
	        return n*factorielle(n-1);
	    else
	        return 1;
	}
	/**
	 * 2eme Methode
	 * @param n
	 * @return
	 */
	public static int factorielleIterative(int n) {
	    int produit = 1;
	    for (int i=1; i<=n; i++)
	        produit *= i;
	    return produit;
	}

	public static void main(String[] args) {
		Factorielle fact =new Factorielle ();
		System.out.println("Factorielle de =" + fact.factorielle(10));
	}

}