/**
 * BEN ABDERRAHMANE IBRAHIM
 * TELECOM II
 * serie 02
 * TP JAVA LE 15/02/2016
 */
package org.Ben_Abderrahmane.coursJava.tps.serie2.Exo2;
import java.util.Arrays;

import org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4.Marin;


public class Equipage{
	/** initialisation du tableau**/
	private Marin [] marins= new Marin[5];
	/**
	 * une methode qui renvoie le NBMAX de marins d'un Equipage
	 * @return
	 */
	public Equipage()
	{
		 marins= new Marin[5];
	}
	public int getCapacite(){
		int NBMAX_Marins=0;
		NBMAX_Marins=marins.length;
		return NBMAX_Marins;
	}
	
	
	/**
	 * une methode qui retourne le Nb r�el des marins dans un Equipage
	 * @return
	 */

	public int getNombreMarins(){
		int NB_Marins=0,i;
		for(i=0;i<marins.length;i++)
			if(marins[i]!=null)
				NB_Marins++;
		return NB_Marins;
	}
/**
 * la methode qui permet d'ajouter un marin � l'Equipage
 * si le tableau est complet alors on renvois false
 * @return
 */

	public boolean addMarinVersion1(Marin marin){
		if (getNombreMarins()==marins.length)
			return false;
		for(Marin o:marins){
			if((o!=null)&&(o.equals(marin))){
				return false;
			}
		}
		for(int i=0;i<getCapacite();){
			if(marins[i]==null)
				marins[i]=marin;
				i++;
				return true;
				
		}
		return false;
	}
	public boolean addMarinVersion2(Marin marin){
		if(getNombreMarins()<getCapacite()){
			if(!isMarinPresent(marin)){
				for(int i=0;i<getCapacite();i++){
					if(marins[i]==null){
						marins[i]=marin;
						return true;
					}
				}
			}
		}
		return false;
	}
	/*public String toString() {
		return "Equipage [marins=" + Arrays.toString(marins) + "]";
	}*/
	public String toString() {
		int i;
		String s="";
		for(i=0;i<getCapacite();i++){
			if(marins[i]!=null){
				s=s+marins[i].toString()+"\n";
			}
		}
		return s;
	}
	/**
	 * une methode qui permet de v�rifier si un marin existe dans l'�quipage
	 * @param marin
	 * @return
	 */
	public boolean isMarinPresent(Marin marin){
		int i;
		if(marins.length!=0){
			for(i=0;i<getCapacite();i++){
				
				if((marins[i]!=null && (marins[i].equals(marin)==true)))
					return true;
			}
		}
		return false;
	}
	/**
	 * une methode qui permet de supprimer un marin dans l'�quipage s'il existe
	 * @param marin
	 * @return
	 */
	public boolean removeMarin(Marin marin){
		int i;
		if(marins.length!=0){
			for(i=0;i<getCapacite();i++){
				if(marins[i].equals(marin)==true){
					marins[i]=null;
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * une methode qui retourne un tableau de marins pr�sents dans l'�quipage
	 * @return
	 */
	public Marin[] getEquipage(){
		Marin[] equipage=new Marin[getNombreMarins()];
		int i,j=0;
		if(marins.length!=0){
			for(i=0;i<getCapacite();i++){
				
					if(marins[i]!=null){
						equipage[j]=marins[i];
						j++;
					}
			}
		}
		return equipage;
	}
	/**
	 * la methode qui efface le contenu de l'�quipage
	 */
	public void clearEquipage(){
		int i;
		if(getCapacite()!=0){
			for(i=0;i<getCapacite();i++){
				marins[i]=null;
			}
		}
	}
	/**
	 * une methode qui pemet de rajouter des membre d'un �quipage
	 * @param equipage
	 * @return
	 */
	public boolean addAllEquipage(Equipage equipage){
		int cpt=0;
		if((this.getNombreMarins())+equipage.getNombreMarins()>=5){
			return false;
		}
		if(equipage.getNombreMarins()<equipage.getCapacite()){
			for(int i=0;i<equipage.getCapacite();i++){
				if(this.isMarinPresent(equipage.marins[i])==true){
					cpt=cpt+1;
					return false;
				}
			}
			if(cpt==0){
				for(int i=0;i<equipage.getCapacite();i++){
					addMarinVersion2(equipage.marins[i]);
				}
			}
		}
	return false;
	}
	/**
	 * une methode qui modifie la taille de l'equipage
	 * @param taille
	 */
	public void etendEquipage(int taille){
		int lengthExtended=this.getCapacite()+taille;
		Marin[] newmarins=new Marin[lengthExtended];
		System.arraycopy(marins, 0, newmarins, 0,getCapacite());
		this.marins=newmarins;
	}
	/**
	 * methode equals pour l'equipage
	 * @param equipage
	 * @return
	 */
	public boolean equalsEquipage(Equipage equipage){
		int cpt=0;
		if((this.getNombreMarins()!=equipage.getNombreMarins())){
			return false;
		}
		else{
			for(int i=0;i<getCapacite();i++){
				if(this.isMarinPresent(equipage.marins[i])){
					cpt=cpt+1;
				}
			}
			if(cpt==getNombreMarins()){
				return true;
			}
		}
		return false;
	}
	
	public int hashCode(){
		final int prime=31;
		int result=1;
		result=prime*result+Arrays.hashCode(marins);
		return result;
	}
	
}
