package org.Ben_Abderrahmane.coursJava.tps.serie2.Exo2;

import org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4.Marin;

public class Main {

	public static void main(String[] args) {
		System.out.println("*************************************************************************");
		System.out.println("             TP JAVA SERIEII           ");
		Equipage equipage=new Equipage();
		Equipage equipage1=new Equipage();
		//int taille=5;
		/*1er equipage*/
		Marin m1= new Marin("BEN ABDERRAHMANE","IBRAHIM",2000);
		Marin m2= new Marin("tall","haguibou",1800);
		Marin m3= new Marin("mukhtar","saqab",1900);
		Marin m4= new Marin("diallo","abdoulaye",2000);
		Marin m5= new Marin("diop","monzon",2100);
		/*1er equipage*/
		Marin m6= new Marin("faye","ibrahima",1750);
		Marin m7= new Marin("voudounon","obed",1900);
		Marin m8= new Marin("diop","helene",1300);
		Marin m9= new Marin("fall","fatou",1200);
		Marin m10= new Marin("konate","ismail",1450);
		
		equipage.addMarinVersion2(m1);
		equipage.addMarinVersion2(m2);
		equipage.addMarinVersion2(m3);
		equipage.addMarinVersion2(m4);
		equipage.addMarinVersion2(m5);
		
		equipage1.addMarinVersion2(m6);
		equipage1.addMarinVersion2(m7);
		equipage1.addMarinVersion2(m8);
		equipage1.addMarinVersion2(m9);
		equipage1.addMarinVersion2(m10);

		//test de la methode getCapacite et getNombreMarins
		System.out.println("la capacit� maximale de l'�quipage est: "+ equipage.getCapacite());
		System.out.println("le nombre de maris effectivement presents dans l'�quipage: "+ equipage.getNombreMarins()+"\n");
		
		System.out.println("la capacit� maximale de l'�quipage 1 est: "+ equipage1.getCapacite());
		System.out.println("le nombre de marins effectivement presents dans l'�quipage 1: "+ equipage1.getNombreMarins()+"\n");
		//test de la methode remove
		equipage.removeMarin(m2);
		System.out.println("la nouvelle taille de l'�quipage est: "+ equipage.getNombreMarins()+"\n");
		//test de la methode isMarinPresent
		if(equipage.isMarinPresent(m1)){
			System.out.println("le marin suivant "+m1 +"existe dans l'�quipage \n");
		}
		
		//test de la methode equals
		if(equipage.equalsEquipage (equipage1)==true){
			System.out.println("les deux equipages sont les m�mes");
		}
		else{
			System.out.println("les deux equipages sont diff�rents");
		}
		//test de la methode getEquipage
		int i;
		for(i=0;i<equipage.getEquipage().length;i++){
			System.out.println(""+equipage.getEquipage()[i]);
		}
		//test de la methode clear
		equipage1.clearEquipage();
		System.out.println("le nombre de marins effectivement presents dans l'�quipage 1: "+ equipage1.getNombreMarins()+"\n");
		

		System.out.println("\n");
		System.out.println("*********************************************************");
		//test de la methode toString
		System.out.println(""+ equipage.toString()+"\n");
		
		//test de la method hashcode
		System.out.println(""+ equipage.hashCode());
		System.out.println(""+ equipage1.hashCode()+"\n");
	

	}

}
