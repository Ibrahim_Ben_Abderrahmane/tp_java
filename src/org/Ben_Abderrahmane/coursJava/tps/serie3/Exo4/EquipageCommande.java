/**
 * BEN ABDERRAHMANE IBRAHIM
 * TELECOM II
 * serie 03 exo4
 * TP JAVA LE 16/02/2016
 */
package org.Ben_Abderrahmane.coursJava.tps.serie3.Exo4;
import org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4.Marin;
import org.Ben_Abderrahmane.coursJava.tps.serie2.Exo2.Equipage;
import org.Ben_Abderrahmane.coursJava.tps.serie3.Exo3.Capitaine;

	public class EquipageCommande extends Equipage{
	private	Capitaine Commandant;
	
		public EquipageCommande(Capitaine commandant){
			super();
			this.Commandant=commandant;
		}
		public Capitaine getCommandant() {
			return Commandant;
		}
		public void setCommandant(Capitaine commandant) {
			Commandant = commandant;
		}
		public static void main(String[] args) {
			//dans cet exemple on montre qu'on ne peut pas mettre un marin 
			//� la place d'un capitaine m�me si on le caste 
			/*
			Capitaine commandant=(Capitaine) new Marin("ibrhim", "ben", 2500);
			*/
			Capitaine commandant= new Capitaine("ibrhim", "ben","Commandant", 2500);
			EquipageCommande E1=new EquipageCommande(commandant);
			System.out.println(""+commandant);
			//mettre en pratique la methode clone de Marin
			Marin m1= new Marin("tall","haguibou",1800);
			Marin m2;
	        try {
				 m2= (Marin) m1.clone();
	        } catch (CloneNotSupportedException e) {
	        	return;
	        }
	        System.out.println("EquipageCommande par ="+E1);
	        System.out.println("m2 ="+m2);
	        System.out.println("m1 ="+m1);
	        System.out.println("***********************************");
	        m1.augmenteSalaire(20);
	        System.out.println(m2);
	        System.out.println("modification du salaire du m1 " +m1);
	        
		}

}
