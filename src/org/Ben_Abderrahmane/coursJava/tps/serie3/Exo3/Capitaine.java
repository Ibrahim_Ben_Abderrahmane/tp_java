/**
 * BEN ABDERRAHMANE IBRAHIM
 * TELECOM II
 * serie 03 exo3
 * TP JAVA LE 16/02/2016
 */
package org.Ben_Abderrahmane.coursJava.tps.serie3.Exo3;

import org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4.Marin;
import org.Ben_Abderrahmane.coursJava.tps.serie2.Exo2.Equipage;

public class Capitaine extends Marin{
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private String grade;
		/**
		 * 
		 * @param nom
		 * @param prenom
		 * @param grade
		 * @param salaire
		 */
		public Capitaine(String nom,String prenom,String grade,int salaire){
			super (nom, prenom, salaire);
			this.setGrade(grade);
	}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + ((grade == null) ? 0 : grade.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			Capitaine other = (Capitaine) obj;
			if (grade == null) {
				if (other.grade != null)
					return false;
			} else if (!grade.equals(other.grade) || super.equals(other))
				return false;
			return true;
		}
		public String getGrade() {
			return grade;
		}
		public void setGrade(String grade) {
			this.grade = grade;
		}
		/**
		 * 
		 */
		public String toString() {
			return "Capitaine [nom=" +getNom() + ", prenom=" + getPrenom() + ",grade=" +getGrade()+", salaire=" + salaire + "]";
		}
	public static void main(String[] args) {
			
		Equipage equipage=new Equipage();
		Capitaine capitaine=new Capitaine("ibrhim", "ben", "capitaine", 2500);
		equipage.addMarinVersion2(capitaine);
		Marin m2= new Marin("tall","haguibou",1800);
		Marin m3= new Marin("mukhtar","saqab",1900);
		Marin m4= new Marin("ibrhim","ben",2500);
		
		equipage.addMarinVersion2(m2);
		equipage.addMarinVersion2(m3);
		
		//test de la methode getEquipage
		int i;
		for(i=0;i<equipage.getEquipage().length;i++){
			System.out.println(""+equipage.getEquipage()[i]);
		}
		System.out.println(""+capitaine.equals(m4));
	}

}
