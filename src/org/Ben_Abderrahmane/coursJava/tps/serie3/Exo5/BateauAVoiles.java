package org.Ben_Abderrahmane.coursJava.tps.serie3.Exo5;

import org.Ben_Abderrahmane.coursJava.tps.serie3.Exo4.EquipageCommande;

public class BateauAVoiles extends Bateau {
	
	public BateauAVoiles(String nom, int tonnage, EquipageCommande equipage) {
		super(nom, tonnage, equipage);
	}

	//methode
	public String  getPropulsion(){
		String propulsionType = "Voiles";
		return propulsionType;
	}
}