package org.Ben_Abderrahmane.coursJava.tps.serie3.Exo5;

import org.Ben_Abderrahmane.coursJava.tps.serie3.Exo4.EquipageCommande;

public class BateauARames extends Bateau {
	//constructeur
	public BateauARames(String nom, int tonnage, EquipageCommande equipage) {
		super(nom, tonnage, equipage);
	}
	//methode
	public String  getPropulsion(){
		String propulsionType = "rames";
		return propulsionType;
	}
	
}

