package org.Ben_Abderrahmane.coursJava.tps.serie3.Exo5;

import org.Ben_Abderrahmane.coursJava.tps.serie3.Exo4.EquipageCommande;

public class BateauAMoteur extends Bateau {
	public BateauAMoteur(String nom, int tonnage, EquipageCommande equipage) {
		super(nom, tonnage, equipage);
	}
	//methode
	public String  getPropulsion(){
		String propulsionType = "Moteur";
		return propulsionType;
	}

	
}