package org.Ben_Abderrahmane.coursJava.tps.serie3.Exo5;
import org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4.Marin;
import org.Ben_Abderrahmane.coursJava.tps.serie2.Exo2.Equipage;
import org.Ben_Abderrahmane.coursJava.tps.serie3.Exo3.Capitaine;
import org.Ben_Abderrahmane.coursJava.tps.serie3.Exo4.EquipageCommande;

public class Bateau implements interfaceBateau {
	private String nom;
	private int tonnage;
	private EquipageCommande equipage;
	public Bateau(String nom,int tonnage,EquipageCommande equipage){
	this.nom=nom;
	this.tonnage=tonnage;
	this.equipage=equipage;
	}
	
	/**
	 * les champs nom et tonnage sont en lecture seule
	 * @return
	 */
	public String getNom() {
		return nom;
	}

	public int getTonnage() {
		return tonnage;
	}
	
	public void setEquipage(EquipageCommande equipage) {
		this.equipage = equipage;
	}
	public EquipageCommande getEquipage() {
		return equipage;
	}
	/**
	 * la methode qui permet de voir si deux bateaux ont le m�me nom ou pas
	 * @param bateau
	 * @return
	 */
	public boolean equals(Bateau bateau) {
		if(this.nom!=null){
			if(bateau.getNom()!=null){
				if(this.nom.equals(bateau.getNom())){
					return true;
				}
			}
		}
			return false;
	}

	@Override
	public String toString() {
		return "Bateau \n nom=" + nom + ", tonnage=" + tonnage + "\n Capitaine="+equipage.getCommandant().toString()+"\n equipage\n" + equipage +"propulsionType="+ getPropulsion();
	}
	
	@Override
	public String getPropulsion() {
		// TODO Auto-generated method stub
		return null;
	}
	

	public static void main(String[] args) {
		//creation de l'equipage de type EquipageCommande
		Capitaine commandant1= new Capitaine("ibrahim", "ben abderrahmane","Commandant", 2500);
		Capitaine commandant2= new Capitaine("faye","ibrahima","Commandant", 1750);
		//creation de equipages
		Equipage equipage1=new Equipage();
		Equipage equipage2=new Equipage();
		/*1er equipage*/
		Marin m1= new Marin("BEN ABDERRAHMANE","IBRAHIM",2500);
		Marin m2= new Marin("tall","haguibou",1800);
		Marin m3= new Marin("mukhtar","saqab",1900);
		Marin m4= new Marin("diallo","abdoulaye",2000);
		Marin m5= new Marin("diop","monzon",2100);
		
		equipage1.addMarinVersion2(m1);
		equipage1.addMarinVersion2(m2);
		equipage1.addMarinVersion2(m3);
		equipage1.addMarinVersion2(m4);
		equipage1.addMarinVersion2(m5);
		/*2er equipage*/
		Marin m6= new Marin("faye","ibrahima",1750);
		Marin m7= new Marin("voudounon","obed",1900);
		Marin m8= new Marin("diop","helene",1300);
		Marin m9= new Marin("fall","fatou",1200);
		Marin m10= new Marin("konate","ismail",1450);
				
		equipage2.addMarinVersion2(m6);
		equipage2.addMarinVersion2(m7);
		equipage2.addMarinVersion2(m8);
		equipage2.addMarinVersion2(m9);
		equipage2.addMarinVersion2(m10);
		
		EquipageCommande E1=new EquipageCommande(commandant1);
		E1.addMarinVersion2(m1);
		E1.addMarinVersion2(m2);
		E1.addMarinVersion2(m3);
		E1.addMarinVersion2(m4);
		E1.addMarinVersion2(m5);
		EquipageCommande E2=new EquipageCommande(commandant2);
		E2.addMarinVersion2(m6);
		E2.addMarinVersion2(m7);
		E2.addMarinVersion2(m8);
		E2.addMarinVersion2(m9);
		E2.addMarinVersion2(m10);

		//creation bateau avec nom tonnage et equipage comme param�tre
		Bateau bateau1=new BateauAVoiles("perleNoire",1000,E1);
		Bateau bateau2=new BateauARames("leNegrier",12000,E2);
		System.out.println(""+bateau1);
		System.out.println("************************************************************************");
		System.out.println(""+bateau2);
	}
}
