package org.Ben_Abderrahmane.coursJava.tps.serie6.Exo12;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.Ben_Abderrahmane.coursJava.tps.serie6.Exo11.Voyage;

public class Main {
	/**
	 * 
	 * @param nomFichier
	 */
	public static void lisFichierText(String nomFichier){

		String chaine="";
		String fichier="fichier.txt";
		//lecture du fichier texte	
		try{
			InputStream ips=new FileInputStream(nomFichier); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String ligne;
			while ((ligne=br.readLine())!=null){
				System.out.println(ligne);
				chaine+=ligne+"\n";
			}
			br.close(); 
		}		
		catch (Exception e){
			System.out.println(e.toString());
		}
	
		//cr�ation ou ajout dans le fichier texte
		try {
			FileWriter fw = new FileWriter (fichier);
			BufferedWriter bw = new BufferedWriter (fw);
			PrintWriter fichierSortie = new PrintWriter (bw); 
				fichierSortie.println (chaine+"\n test de lecture et �criture !!"); 
			fichierSortie.close();
			//System.out.println("Le fichier " + fichier + " a �t� cr��!"); 
		}
		catch (Exception e){
			System.out.println(e.toString());
		}		
	}
	public static List<Voyage> lisFichier(String nomFichier){
		
		StringTokenizer separator = null;
		Voyage v;
		List<Voyage> voyages= new ArrayList<Voyage>();
		try{
			InputStream in_put_stream=new FileInputStream(nomFichier); 
			InputStreamReader in_put_stream_reader=new InputStreamReader(in_put_stream);
			BufferedReader buffer_reader=new BufferedReader(in_put_stream_reader);
			String ligne;
			while ((ligne=buffer_reader.readLine())!=null){
				separator=new StringTokenizer(ligne,"|");
				v=new Voyage(separator.nextToken(),separator.nextToken(),Integer.parseInt(separator.nextToken()));
				System.out.print(v);
			}
			buffer_reader.close(); 
			return voyages;
		}		
		catch (Exception e){
			System.out.println(e.toString());
			return null;
		}
	}
	public static void main(String[] args) {
		//Main.lisFichierText("Voyages.txt");
		Main.lisFichier("Voyages.txt");
	}

}
