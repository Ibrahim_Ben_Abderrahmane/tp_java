package org.Ben_Abderrahmane.coursJava.tps.serie6.Exo11;

import java.util.ArrayList;
import java.util.List;

public class Voyage {
	private String prt_depart;
	private String prt_destination;
	private int nbr_jours;
	static List<Voyage> voyages=new ArrayList<Voyage>();
	/**
	 * 
	 * @param prt_depart
	 * @param prt_destination
	 * @param nbr_jours
	 */
	public Voyage(String prt_depart,String prt_destination,int nbr_jours){
		this.prt_depart=prt_depart;
		this.prt_destination=prt_destination;
		this.nbr_jours=nbr_jours;
		voyages.add(this);
	}
	/**
	 * 
	 * @return
	 */
	public String getPrt_depart() {
		return prt_depart;
	}
	/**
	 * 
	 * @return
	 */
	public String getPrt_destination() {
		return prt_destination;
	}
	/**
	 * 
	 * @return
	 */
	public int getNbr_jours() {
		return nbr_jours;
	}
	/**
	 * 
	 * @param depart
	 * @param destination
	 * @return
	 */
	public static Voyage getVoyage(String depart,String destination){
		for(Voyage v:voyages){
			if( (v.getPrt_depart().equals(depart) && 
					v.getPrt_destination().equals(destination)) ||
					(v.getPrt_depart().equals(destination) &&
							v.getPrt_destination().equals(depart))){
				return v;
			}
		}
		return null ;
	}
	/**
	 * 
	 * @param depart
	 * @return
	 */
	public static  List<Voyage> getVoyageAuDepartDe(String depart){
		List<Voyage> liste=new ArrayList<Voyage>();
		for(int i=0;i<voyages.size();i++){
			if(voyages.get(i).getPrt_depart().equals(depart)){
				Voyage v=voyages.get(i);
				liste.add(v);
			}
		}
		return liste;	
	}
	public static Voyage getPlusCourtAuDepartDe(String depart){
	
		int traversee_min=1000;
		for(int i=0;i<voyages.size();i++){
			if(voyages.get(i).getPrt_depart().equals(depart)){
				if(voyages.get(i).getNbr_jours()<traversee_min){
					traversee_min=voyages.get(i).getNbr_jours();
				}
			}
		}
		for(int i=0;i<voyages.size();i++){
			if(voyages.get(i).getNbr_jours()==traversee_min){
				return voyages.get(i);
			}
		}
		return null;
	}
	/**
	 * 
	 * @param depart
	 * @param arrivee
	 * @param nombreJours
	 * @return
	 */
	public static Voyage newInstance(String depart, String arrivee,int nombreJours){
		if(getVoyage(depart,arrivee)!=null){
			System.out.println("le voyage existe d�j�"+"\n");
			return getVoyage(depart,arrivee);
		}
		else{
			System.out.println("Le voyage au d�part de "+depart +" � destination de "+arrivee+ " n existe pas."+"\n");
			Voyage v=new Voyage(depart,arrivee,nombreJours);
			return v;
		}
	}

	public String toString() {
		return "Voyage  prt_depart:" + prt_depart + "\t"+", prt_destination:" + prt_destination +"\t"+ ", nbr_jours:" + nbr_jours
				+ "\n";
	}
	public static void main(String[] args) {
		Voyage v1=new Voyage("Alger","Marseille",3);
		Voyage v2=new Voyage("Alger","Oran",1);
		Voyage v3=new Voyage("Alger","Anaba",1);
		Voyage v4=new Voyage("Marseille","Anaba",4);
		Voyage v5=new Voyage("Marseille","Oran",5);
		Voyage v6=new Voyage("Marseille","Alger",3);

		System.out.println(Voyage.voyages);
		
		System.out.println("***********************************************************************");
		System.out.println("test de la fonction  getVoyageAuDepartDe\n");
		System.out.println(Voyage.getVoyageAuDepartDe("Marseille"));
		System.out.println("***********************************************************************");
		
		System.out.println("test de la fonction  getVoyage\n");
		System.out.println(Voyage.getVoyage("Marseille","Oran"));
		System.out.println("***********************************************************************");
		
		System.out.println("test de la fonction getPlusCourtAuDepartDe\n");
		System.out.println(Voyage.getPlusCourtAuDepartDe("Alger"));
		System.out.println("***********************************************************************");
		
		System.out.println("test de la fonction newInstance\n");
		System.out.println(Voyage.newInstance("dakar","Alicante",7));
		System.out.println("***********************************************************************");
		
		System.out.println("La nouvelle liste est :");
		System.out.println(Voyage.voyages);
		
	}
}
	
