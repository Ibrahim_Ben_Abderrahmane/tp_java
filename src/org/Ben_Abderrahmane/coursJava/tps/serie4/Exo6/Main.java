package org.Ben_Abderrahmane.coursJava.tps.serie4.Exo6;
import org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4.Marin;

public class Main {

	public static void main(String[] args) {
		
		listEquipage LesAiglesNoirs=new listEquipage();
		listEquipage LesScorpions=new listEquipage();
		
		Marin m1= new Marin("BEN ABDERRAHMANE","IBRAHIM",2000);
		Marin m2= new Marin("tall","haguibou",1800);
		Marin m3= new Marin("mukhtar","saqab",1900);
		Marin m4= new Marin("diallo","abdoulaye",2000);

		
		LesAiglesNoirs.addMarin(m1);
		LesAiglesNoirs.addMarin(m2);
		
		LesScorpions.addMarin(m3);
		LesScorpions.addMarin(m4);
		
		System.out.println("************LesAiglesNoirs*************");
		System.out.println(LesAiglesNoirs);
		System.out.println("************LesScorpions*************");
		System.out.println(LesScorpions);
		
		LesAiglesNoirs.addAllEquipage(LesScorpions);
		System.out.println("************le nouvel equipage: LesAiglesNoirs*************");
		System.out.println(LesAiglesNoirs);
		
		System.out.println("on compare deux equipages differents"+"\n");
		System.out.println(LesAiglesNoirs.equals(LesScorpions));
		
		System.out.println("on compare un equipage � lui m�me"+"\n");
		System.out.println(LesAiglesNoirs.equals(LesAiglesNoirs));
		
		LesScorpions.getEquipage().set(0, m4);
		System.out.println(LesScorpions);
	}
	

}
