package org.Ben_Abderrahmane.coursJava.tps.serie4.Exo6;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4.Marin;


public class listEquipage {
	
	List<Marin> liste = new ArrayList<Marin>();
	public listEquipage()
	{
		List<Marin> liste = new ArrayList<Marin>();
	}
	/**
	 *  une methode qui renvoie le NBMAX de marins d'un Equipage
	 * @return
	 */
	public int getCapacite(){
		int NBMAX_Marins=0;
		NBMAX_Marins=liste.size();
		return NBMAX_Marins;
	}
	/**
	 * une methode qui retourne le Nb r�el des marins dans un Equipage
	 * @return
	 */

	public int getNombreMarins(){
		Iterator<Marin> iterateur=liste.iterator();
		int NB_Marins=0,i;
		for(i=0;i<liste.size();i++)
			if(iterateur.hasNext()!=false)
				NB_Marins++;
		return NB_Marins;
	}
	
	public boolean addMarin(Marin marin){
				return liste.add(marin);
	}
	@Override
	public String toString() {
		return "listEquipage liste= \n" + liste +"";
	}
	/**
	 * une methode qui permet de supprimer un marin dans l'�quipage s'il existe
	 * @param marin
	 * @return
	 */
	public boolean removeMarin(Marin marin){
		if(liste.contains(marin)==true){
			liste.remove(marin);
			return true;
		}
		return false;
	}
	/**
	 * une methode qui retourne un tableau de marins pr�sents dans l'�quipage
	 * @return
	 */
	public  List<Marin> getEquipage(){
		return liste;
	}
	/**
	 * la methode qui efface le contenu de l'�quipage
	 */
	public void clearEquipage(){
		if(getCapacite()!=0){
			liste.clear();
		}
	}
	/**
	 * une methode qui pemet de rajouter des membre d'un �quipage
	 * @param equipage
	 * @return
	 */
	public boolean addAllEquipage(listEquipage equipage){
		int i,cpt=0;

		for(i=0;i<liste.size();i++){
			if(liste.contains(equipage.getEquipage().get(i))==true){
				cpt=cpt++;
			}
		}
		if(cpt==0){
			liste.addAll(getCapacite(), equipage.getEquipage());
		}
		return false;
	}
	/**
	 * methode equals pour l'equipage
	 * @param equipage
	 * @return
	 */

	public boolean equalsEquipage(listEquipage equipage){
		int i,cpt=0;
		for(i=0;i<equipage.getCapacite();i++){
			if(liste.contains(equipage.getEquipage().get(i))==true){
				cpt++;
			}
		}
		if(cpt==equipage.getCapacite()){
			return true;
		}
		return false;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((liste == null) ? 0 : liste.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		listEquipage other = (listEquipage) obj;
		if (liste == null) {
			if (other.liste != null)
				return false;
		} else if (!liste.equals(other.liste))
			return false;
		return true;
	}
	
}
