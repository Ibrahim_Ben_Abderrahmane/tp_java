package org.Ben_Abderrahmane.coursJava.tps.serie4.Exo9;

import java.util.SortedSet;
import java.util.TreeSet;

import org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4.Marin;
import org.Ben_Abderrahmane.coursJava.tps.serie4.Exo8.ComparableMarin;

public class Tri_Equipage {
	private static SortedSet<ComparableMarin> marins;
	
	public Tri_Equipage(){
		marins=new TreeSet<ComparableMarin>();
	}
	/**
	 * 
	 * @param nom
	 * @return
	 */
	public static Marin getByName(String nom){
		for(Marin m: marins){
			if(m.getNom().equals(nom)){
				return m;
			}
		}
		return null;
	}
	/**
	 * 
	 * @param marin
	 * @return
	 */
	public boolean isMarinPresent(Marin marin){
		return Tri_Equipage.marins.contains(marin);
	
	}
	/**
	 * 
	 * @param marin
	 * @return
	 */
	public boolean addMarin(ComparableMarin marin){
		if(isMarinPresent(marin)==false){
			marins.add(marin);
			return true;
		}
		return false;
	}
	@Override
	public String toString() {
		return "Tri_Equipage marin:" + marins + "\n";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((marins == null) ? 0 : marins.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		if (marins == null) {
			if (Tri_Equipage.marins != null)
				return false;
		} else if (!marins.equals(Tri_Equipage.marins))
			return false;
		return true;
	}
/**
 * 
 * @param args
 */
	public static void main(String[] args) {
		Tri_Equipage equipage_trie= new Tri_Equipage();
		ComparableMarin m1= new ComparableMarin("BEN ABDERRAHMANE","IBRAHIM",2000);
		ComparableMarin m2= new ComparableMarin("tall","haguibou",1800);
		ComparableMarin m3= new ComparableMarin("Aguibelle","tall",1800);
		ComparableMarin m4= new ComparableMarin("BEN ABDERRAHMANE","BRAHIM",1800);

		equipage_trie.addMarin(m1);//ajout � la liste le marin 1
		equipage_trie.addMarin(m2);//ajout � la liste le marin 2
		equipage_trie.addMarin(m3);//ajout � la liste le marin 3
		equipage_trie.addMarin(m4);//ajout � la liste le marin 4
		System.out.println(equipage_trie);
		
		System.out.println(getByName("Aguibelle"));
	
	}

}
