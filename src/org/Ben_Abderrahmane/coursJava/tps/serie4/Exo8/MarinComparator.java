package org.Ben_Abderrahmane.coursJava.tps.serie4.Exo8;

import java.util.Comparator;
import org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4.Marin;

public class MarinComparator implements Comparator<Marin>{
	
	public int compare(Marin marin1, Marin marin2) {
		int order=marin1.getNom().compareTo(marin2.getNom());
		if(order!=0){
			return order;
		}
		else{
			order=marin1.getPrenom().compareTo(marin2.getPrenom());
		}
		return order;
	}
	public static void main(String[] args) {
		
		MarinComparator C1= new MarinComparator();
		
		Marin m1= new Marin("BEN ABDERRAHMANE","IBRAHIM",2000);
		Marin m2= new Marin("tall","haguibou",1800);
		System.out.println(C1.compare(m1,m2));
	}

	
}
