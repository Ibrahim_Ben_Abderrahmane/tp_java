package org.Ben_Abderrahmane.coursJava.tps.serie4.Exo8;

import java.util.SortedSet;
import java.util.TreeSet;

import org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4.Marin;

public class ComparableMarin extends Marin implements Comparable<Marin>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ComparableMarin(String nouveauNom, String nouveauPrenom, int nouveauSalaire) {
		super(nouveauNom, nouveauPrenom, nouveauSalaire);
	}

	@Override
	public int compareTo(Marin marin1) {
		int order=this.getNom().compareTo(marin1.getNom());
		if(order!=0){
			return order;
		}
		else{
			order=this.getPrenom().compareTo(marin1.getPrenom());
		}
		return order;
	}
	public static void main(String[] args) {
		
		SortedSet<ComparableMarin> set= new TreeSet<ComparableMarin>();
		ComparableMarin m1= new ComparableMarin("BEN ABDERRAHMANE","IBRAHIM",2000);
		ComparableMarin m2= new ComparableMarin("tall","haguibou",1800);
		ComparableMarin m3= new ComparableMarin("Aguibelle","tall",1800);
		ComparableMarin m4= new ComparableMarin("BEN ABDERRAHMANE","BRAHIM",1800);
		set.add(m1);//ajout � la liste le marin 1
		set.add(m2);//ajout � la liste le marin 2
		set.add(m3);//ajout � la liste le marin 3
		set.add(m4);//ajout � la liste le marin 4
		System.out.println(set);
	
	}
	
}
