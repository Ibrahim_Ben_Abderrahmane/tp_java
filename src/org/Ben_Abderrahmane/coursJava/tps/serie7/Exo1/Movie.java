package org.Ben_Abderrahmane.coursJava.tps.serie7.Exo1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.Ben_Abderrahmane.coursJava.tps.serie1.Exo4.Marin;
import org.Ben_Abderrahmane.coursJava.tps.serie6.Exo11.Voyage;
import org.Ben_Abderrahmane.coursJava.tps.serie6.Exo12.Main;
import org.Ben_Abderrahmane.coursJava.tps.serie7.Exo1.Acteur;
/**
 * 
 * @author Ben abderrahmane
 *
 */
public class Movie {
	private String titre;
	private int annee;
	
	static List<Movie> films=new ArrayList<Movie>();
	static List<Acteur> actors=new ArrayList<Acteur>();
	
	public Movie(String titre,int annee,List<Acteur> acteurs){
		this.titre=titre;
		this.annee=annee;
		films.add(this);
		Movie.actors.addAll(0,acteurs);
		
	}
	/**
	 * 
	 * @return
	 */
	public String getTitre() {
		return titre;
	}
	/**
	 * 
	 * @param titre
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}
	/**
	 * 
	 * @return
	 */
	public int getAnnee() {
		return annee;
	}
	/**
	 * 
	 * @param annee
	 */
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	/**
	 * 
 	 * @return
	 */
	public static List<Movie> getFilms() {
		return films;
	}
	/**
	 * 
	 * @param films
	 */
	public static void setFilms(List<Movie> films) {
		Movie.films = films;
	}
	/**
	 * 
	 * @param nomFichier
	 * @return
	 */
	public static Movie lisFichier(String nomFichier){
		StringTokenizer separator = null;
		Movie film=null;
		List<Acteur> Acteurs_liste= new ArrayList<Acteur>();
		try{
			//InputStream in_put_stream=new FileInputStream(nomFichier); 
			//InputStreamReader in_put_stream_reader=new InputStreamReader(in_put_stream);
			BufferedReader buffer_reader=new BufferedReader(new FileReader(nomFichier));
			String ligne;
			String titre=null;
			String annee=null;
			ligne=buffer_reader.readLine();
			int j=0;
			while (ligne!=null){
				separator=new StringTokenizer(ligne,"()//");
				j++;
				System.out.println("le nombre de ligne lu est:"+j+"\n");
				System.out.println("**************************************************");
				titre=separator.nextToken();
				//System.out.println("film:"+titre);
				
				annee=separator.nextToken();
				if (annee.indexOf(",") !=-1)
					annee = annee.substring(0,annee.indexOf(","));
				else 
					
				System.out.println("annee :"+annee+"\n");
				while(separator.hasMoreTokens()){
					
					//String nom1=separator.nextToken();
					String tmp=separator.nextToken();
					if (tmp.indexOf(",")!=-1)
					{
						String nom = tmp.substring(0,tmp.indexOf(","));
						//System.out.println("Nom : "+nom);
						String prenom = tmp.substring(tmp.indexOf(",")+2);
						//System.out.println("Prenom : "+prenom);
						Acteurs_liste.add(new Acteur(nom,prenom));
					}
				
				}
				
				film=new Movie(titre,Integer.parseInt(annee),Acteurs_liste);
				System.out.println(film);
				ligne=buffer_reader.readLine();
			}

			buffer_reader.close(); 
			return film;
		}		
		catch (Exception e){
			System.out.println(e.toString());
			return null;
		}
	}
	/**
	 * 
	 * @param nomFichier
	 */
	public static void lisFichierText(String nomFichier){
		
		String chaine="";
		String fichier="fichier.txt";
		//lecture du fichier texte	
		try{
			InputStream ips=new FileInputStream(nomFichier); 
			InputStreamReader ipsr=new InputStreamReader(ips);
			BufferedReader br=new BufferedReader(ipsr);
			String ligne;
			while ((ligne=br.readLine())!=null){
				System.out.println(ligne);
				chaine+=ligne+"\n";
			}
			br.close(); 
		}		
		catch (Exception e){
			System.out.println(e.toString());
		}
	
		//cr�ation ou ajout dans le fichier texte
		try {
			FileWriter fw = new FileWriter (fichier);
			BufferedWriter bw = new BufferedWriter (fw);
			PrintWriter fichierSortie = new PrintWriter (bw); 
				fichierSortie.println (chaine+"\n test de lecture et �criture !!"); 
			fichierSortie.close();
			//System.out.println("Le fichier " + fichier + " a �t� cr��!"); 
		}
		catch (Exception e){
			System.out.println(e.toString());
		}		
	}
	@Override
	public String toString() {
		return "Movie titre:" + titre + ", annee:" + annee + "Acteurs:"+actors;
	}
	/**
	 * 
	 * @param args
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + annee;
		result = prime * result + ((titre == null) ? 0 : titre.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Movie other = (Movie) obj;
		if (annee != other.annee)
			return false;
		if (titre == null) {
			if (other.titre != null)
				return false;
		} else if (!titre.equals(other.titre))
			return false;
		return true;
	}
	public static void main(String[] args) {
		//Movie.lisFichierText("movies-mpaa.txt");
		Movie.lisFichier("movies-mpaa.txt");
		
		

	}
}
/*La m�thode � indexOf �


- Si s est un String, s.indexOf(�o�) renvoie la premi�re position � partir du d�but o� l�on trouve �o� ds s. Si �o� n�existe pas ds s, on renvoie la valeur -1

Exemple :

s="Bonjour";
s.indexOf(�o�);
// renvoie 1





- s.indexOf(�o�,2) renvoie la premi�re position � partir de la position 2 o� l�on a �o� ds s. Ici c�est 4
 - indexOf peur aussi �tre utilis� avec un argument chaine de caract�re. Le principe reste le m�me
*/
/*La m�thode � substring �


- Si s est un String, s.substring(d) renvoie la ss chaine de s de l�index d jusqu�� sa fin

Exemple : 

s="Bonjour";
s.substring(3);
// renvoie "jour"





- s.substring(d,l) renvoie la ss chaine de s de l�index d jusqu�� d+(l-1)

Exemple : 

s="Bonjour";
s.substring(0,2);
// renvoie "Bo"

*/